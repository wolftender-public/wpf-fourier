﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.Windows.Threading;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Xml.Serialization;
using Microsoft.Win32;

namespace WPF16_fourier {
	public class Circle {
		private float radius;
		private float frequency;

		[XmlIgnore]
		public Ellipse HelperCircle;

		[XmlIgnore]
		public Ellipse PointMarker;

		[XmlIgnore]
		public Line RadiusHelper;

		[XmlIgnore]
		public Point CurrentPoint;

		[XmlIgnore]
		public Point PreviousPoint;

		[XmlIgnore]
		public Point Pivoit;

		public float Radius {
			get { return radius; }
			set { 
				radius = value;
			}
		}

		public float Frequency {
			get { return frequency; }
			set { frequency = value; }
		}

		public Circle () { }

		public Circle (float radius, float frequency) {
			this.radius = radius;
			this.frequency = frequency;
		}
	}

	public partial class MainWindow : Window {
		public ObservableCollection<Circle> collection;
		public DispatcherTimer timer;

		public const int timerInterval = 5;
		public const int timerWaitTotal = 10000;

		private int waited = 0;
		private long lastElapsed = 0;
		private Stopwatch stopwatch;

		private float targetRadius;
		private bool isDrawing = false;
		private bool isLoadingDone = false;

		public MainWindow () {
			InitializeComponent ();

			collection = new ObservableCollection<Circle> ();

			collection.Add (new Circle (50.0f, 0.5f));
			collection.Add (new Circle (100.0f, -1.0f));

			DataContext = collection;

			timer = new DispatcherTimer (DispatcherPriority.Send);
			timer.Tick += timer_Tick;
			timer.Interval = TimeSpan.FromMilliseconds (timerInterval);

			stopwatch = Stopwatch.StartNew ();
		}

		private void Window_Loaded (object sender, RoutedEventArgs e) {
			repaint ();
		}

		private void repaint () {
			canvasMain.Children.Clear ();

			double cx = canvasMain.ActualWidth / 2, cy = canvasMain.ActualHeight / 2;

			foreach (Circle c in collection) {
				c.CurrentPoint = new Point (cx + c.Radius, cy);
				c.PreviousPoint = c.CurrentPoint;

				c.PointMarker = new Ellipse {
					Fill = Brushes.Red,
					Stroke = Brushes.Red,
					Width = 10,
					Height = 10,
					Margin = new Thickness (c.CurrentPoint.X - 5, c.CurrentPoint.Y - 5, 0, 0)
				};

				c.RadiusHelper = new Line {
					X1 = canvasMain.ActualWidth / 2,
					X2 = c.CurrentPoint.X,
					Y1 = canvasMain.ActualHeight / 2,
					Y2 = c.CurrentPoint.Y,
					Stroke = Brushes.Black
				};

				c.HelperCircle = new Ellipse {
					Stroke = Brushes.Black,
					Width = 2 * c.Radius,
					Height = 2 * c.Radius,
					Margin = new Thickness (cx - c.Radius, cy - c.Radius, 0, 0)
				};

				canvasMain.Children.Add (c.PointMarker);
				cx += c.Radius;
			}

			showHelperCircles (menuItemDrawCircles.IsChecked);
			showHelperLines (menuItemDrawLines.IsChecked);
		}

		private void refresh () {
			foreach (Circle c in collection) {
				c.CurrentPoint = new Point (canvasMain.ActualWidth / 2 + c.Radius, canvasMain.ActualHeight / 2);
				c.PointMarker.Margin = new Thickness (c.CurrentPoint.X - 5, c.CurrentPoint.Y - 5, 0, 0);
			}
		}

		private void loadingDone () {
			isLoadingDone = true;
			isDrawing = false;
		}

		private void step () {
			double frac = (double) waited / timerWaitTotal;
			double cx = canvasMain.ActualWidth / 2;
			double cy = canvasMain.ActualHeight / 2;

			for (int i = 0; i < collection.Count; ++i) {
				Circle c = collection [i];
				double tetha = frac * (Math.PI * 2) * c.Frequency;

				double sine = Math.Sin (tetha);
				double cosine = Math.Cos (tetha);

				double px = cx + (c.Radius * cosine);
				double py = cy + (c.Radius * sine);

				c.RadiusHelper.X1 = cx;
				c.RadiusHelper.Y1 = cy;
				c.RadiusHelper.X2 = px;
				c.RadiusHelper.Y2 = py;

				c.HelperCircle.Margin = new Thickness (cx - c.Radius, cy - c.Radius, 0, 0);
				c.PointMarker.Margin = new Thickness (px - 5, py - 5, 0, 0);

				cx = px;
				cy = py;

				c.CurrentPoint = new Point (cx, cy);

				if (i == collection.Count - 1) { // last
					canvasMain.Children.Add (new Line {
						X1 = c.PreviousPoint.X,
						Y1 = c.PreviousPoint.Y,
						X2 = c.CurrentPoint.X,
						Y2 = c.CurrentPoint.Y,
						Stroke = Brushes.Blue,
						StrokeThickness = 3
					});
				}

				c.PreviousPoint = c.CurrentPoint;
			}

			progressMain.Maximum = timerWaitTotal;
			progressMain.Value = waited;
		}

		private void timer_Tick (object sender, EventArgs e) {
			long curr = stopwatch.ElapsedMilliseconds;
			long dt = curr - lastElapsed;

			lastElapsed = curr;

			waited += (int) dt;

			if (waited >= timerWaitTotal) {
				waited = timerWaitTotal;
				timer.Stop ();
				loadingDone ();
			}

			step ();
		}

		private void startAnimation () {
			lastElapsed = stopwatch.ElapsedMilliseconds;
			timer.Start ();
			isDrawing = true;
		}

		private void stopAnimation () {
			timer.Stop ();
		}

		private void resetAnimation () {
			timer.Stop ();
			waited = 0;
			isDrawing = false;
			isLoadingDone = false;
			repaint ();
		}

		private void buttonStart_Click (object sender, RoutedEventArgs e) {
			startAnimation ();
		}

		private void buttonPause_Click (object sender, RoutedEventArgs e) {
			stopAnimation ();
		}

		private void buttonReset_Click (object sender, RoutedEventArgs e) {
			resetAnimation ();
		}

		private void showHelperCircles (bool show) {
			double cx = canvasMain.ActualWidth / 2, cy = canvasMain.ActualWidth / 2;
			foreach (Circle c in collection) {
				if (show) {
					if (c.HelperCircle.Parent == null) canvasMain.Children.Add (c.HelperCircle);
				} else {
					canvasMain.Children.Remove (c.HelperCircle);
				}
			}
		}

		private void showHelperLines (bool show) {
			foreach (Circle c in collection) {
				if (show) {
					if (c.RadiusHelper.Parent == null) canvasMain.Children.Add (c.RadiusHelper);
				} else {
					canvasMain.Children.Remove (c.RadiusHelper);
				}
			} 
		}

		private void menuItemDrawCircles_Click (object sender, RoutedEventArgs e) {
			showHelperCircles (((MenuItem) sender).IsChecked);
		}

		private void menuItemDrawLines_Click (object sender, RoutedEventArgs e) {
			showHelperLines (((MenuItem) sender).IsChecked);
		}

		private void menuItemExit_Click (object sender, RoutedEventArgs e) {
			if (MessageBox.Show ("do you want to exit??", "hey", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes) {
				Close ();
			}
		}

		private void dataGridMain_TargetUpdated (object sender, DataTransferEventArgs e) {
			//MessageBox.Show ("aaaa");
		}

		private void menuItemOpenCommand (object sender, ExecutedRoutedEventArgs e) {
			OpenFileDialog ofd = new OpenFileDialog ();
			ofd.Filter = "XML files (.xml)|*.xml";

			bool? result = ofd.ShowDialog ();

			if (result == true) {
				string filename = ofd.FileName;
				using (System.IO.FileStream stream = System.IO.File.OpenRead (filename)) {
					XmlSerializer serializer = new XmlSerializer (typeof (ObservableCollection<Circle>));
					ObservableCollection<Circle> newCollection = null;

					try {
						newCollection = (ObservableCollection<Circle>) serializer.Deserialize (stream);
					} catch (Exception exception) {
						MessageBox.Show (exception.Message, "invalid xml", MessageBoxButton.OK, MessageBoxImage.Error);
					}

					if (newCollection != null) {
						collection = newCollection;
						DataContext = collection;

						resetAnimation ();
					} else {
						MessageBox.Show ("this is not a valid file, please provide a valid playstation 3 bios image", "invalid", MessageBoxButton.OK, MessageBoxImage.Error);
					}
				}
			}
		}

		private void menuItemSaveCommand (object sender, ExecutedRoutedEventArgs e) {
			SaveFileDialog sfd = new SaveFileDialog ();
			sfd.FileName = "out";
			sfd.DefaultExt = ".xml";
			sfd.Filter = "XML files (.xml)|*.xml";

			bool? result = sfd.ShowDialog ();

			if (result == true) {
				string filename = sfd.FileName;
				using (System.IO.FileStream stream = System.IO.File.OpenWrite (filename)) {
					XmlSerializer serializer = new XmlSerializer (typeof (ObservableCollection<Circle>));
					serializer.Serialize (stream, collection);
				}
			}
		}

		private void dataGridMain_LostFocus (object sender, RoutedEventArgs e) {
			repaint ();
			resetAnimation ();
		}

		private void menuItemNewCommand (object sender, ExecutedRoutedEventArgs e) {
			collection.Clear ();
			resetAnimation ();
		}
	}
}
